# Open Mobility indicator Jupyter notebook Congestion du trafic routier Montpellier Métropole

## description : [indicator.yaml file](https://gitlab.com/open-mobility-indicators/indicators/3M-congestion/-/blob/main/indicator.yaml)

Ce notebook produit des indicateurs mesurant la congestion du trafic routier sous forme de fichier GeoJSON à partir de [l'open data 3M](https://data.montpellier3m.fr/dataset/comptage-vehicules-particuliers-de-montpellier/resource/2002371e-b66a-46a8-a715-d6a93ea72f26).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute) : see the wiki doc.](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)  
